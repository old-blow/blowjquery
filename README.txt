Blowmedia JQuery Extensions
--------------------------------

.loop(method, timer, next, prev)
	
	When applied to an element, loops over all the elements, executing a callback function
	every time the timer expires.

	- Method can be any one of:
		start()
			Starts the loop

		stop()
			Stops the loop

		next()
			Goes to the next element and applies the 'next' callback

		prev()
			Goes to the previous element and applies the 'prev' callback
		
		get_current()
			Returns the current position in the loop

		get_next()
			Returns the next position in the loop

		get_prev()
			Returns the previous position in the loop

	
	- Timer is measured in milliseconds and represents the time between slide transitions 

	- Callback functions take the form:

		function(current, previous)

		Where current is the new slide we're now on and previous is the one we were just on.
		This callback is where we can apply a transitional effect.


.cycle(config)

	Uses loop() and applies a transitional effect

	- Config can have the following fields:
		effect
			The transitional effect to apply

		timer
			The time inbetween each transition

		prev
			Specifies an element that can be clicked to send the loop to the previous
			slide

		next
			Specifies an element that can be clicked to send the loop to the next slide


	The following effects are currenetly allowed:
		
		fader
			Fades out the current slide and fades in the next

		slider
			Slides the current slide to the left whilst fading out. Also slides the
			next slide in whilst fading in.
