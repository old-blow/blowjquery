(($) ->

  ###
  BlowMedia
  A general class containing some useful functions
  ###
  class BM

    ###
    URL Segment
    Returns the segment specified by i after the base URL
    ###
    url_segment: (i) ->
      url = document.URL.split('/')
      url.splice(0, 3)
      url[i]

    ###
    Show Grid
    Attaches an overlay to the body to show the grid
    ###
    show_grid: ->
      containerStyle =
        'width': '960px'
        'margin-left': (($(window).width() / 2) - 480) + 'px'
        'height': $(document).height()
        'position': 'absolute'
        'z-index': 100

      colStyle =
        'width': '60px'
        'margin': '0 10px'
        'float': 'left'
        'background-color': 'pink'
        'opacity': '0.5'
        'height': $(document).height()

      $('body').prepend('<div id="bm-grid"></div>')
      $('#bm-grid').css(containerStyle)

      $('#bm-grid').append('<div class="bm-grid-col"></div>') for i in [0...12]

      $('.bm-grid-col').each ->
        $(@).css colStyle
      return


    ###
    Hide Grid
    Hide the grid overlay
    ###
    hide_grid: ->
      $('#bm-grid').remove()
      return

    ###
    Toggle Grid
    ###
    toggle_grid: ->
      if $('#bm-grid').length > 0
        @hide_grid()
      else
        @show_grid()
      return

    ###
    Snap To
    Calling this will snap the window to any snap points that have been set
    ###
    snap: ->
      $($(window).data('snap_points')).each ->
        if window.scrollY > $(@).offset().top - 300 && window.scrollY < $(@).offset().top + 300
          $.scroll $(@).offset().top
          return

  window.bm = new BM()

  ###
  Loop
  When applied to a collection of elements, will loop through each one
  in turn.
  ###
  $.fn.loop = (cfg) ->

    if (@data 'count') is undefined
      start = 0 if not start?
      @data cfg
      @data 'count', @length
      @data 'current', start

    methods =

      ###
      Start
      The first time the loop is started we don't want to run 'next'
      so this just starts it later.
      ###
      start: =>
        @data 'is_animating', true
        @data 'timer', setTimeout methods.loop, @data 'time'
        return

      ###
      Stop
      Stop the loop from running
      ###
      stop: =>
        @data 'is_animating', false
        clearTimeout @data 'timer'
        return

      ###
      Loop
      This is the main loop that triggers every time the timeout has
      expired.
      ###
      loop: =>
        @data('callback') @data() if @data('callback')?
        methods.loop_next()
        @data 'timer', setTimeout methods.loop, @data 'time'
        return

      ###
      Loop Next
      Moves to the next slide in the sequence
      ###
      loop_next: =>
        prev = methods.get_current()
        next = methods.get_next()
        @data 'current', next
        @data('nextEffect') @eq(next), @eq(prev)
        return

      ###
      Next
      Moves to the next slide in the sequence
      ###
      next: =>
        methods.stop()
        methods.loop()
        return

      ###
      Loop Prev
      Moves to the previous slide in the sequence
      ###
      loop_prev: =>
        cur = methods.get_current()
        prev = methods.get_prev()
        if @data('prevEffect')?
          @data('prevEffect') @eq(prev), @eq(cur)
        else
          @data('nextEffect') @eq(prev), @eq(cur)
        @data('current', prev)
        return

      ###
      Prev
      Moves to the previous slide in the sequence
      ###
      prev: =>
        methods.stop()
        methods.loop_prev()
        methods.start()
        return

      ###
      Get Current
      Gets the current element in the loop.
      ###
      get_current: =>
        @data 'current'

      ###
      Get Previous
      Returns the previous element in the loop
      ###
      get_prev: =>
        if @data('current') is 0
          @data('count') - 1
        else
          @data('current') - 1

      ###
      Get Next
      Returns and sets the current element to be the next element in
      the loop.
      ###
      get_next: =>
        if @data('current') is (@data('count') - 1)
          0
        else
          @data('current') + 1

      ###
      Is Animating
      Returns true if the elements are animating, else false
      ###
      is_animating: =>
        @data 'is_animating'

    # Execute the right method
    methods[cfg.method]()

  ###
  Last
  Returns the last element of an array
  ###
  $.fn.last_ele = ->
    @eq(@eq.length - 1)

  ###
  Fader
  When applied to a collection of elements, will fade elements in one after
  another
  ###
  $.fn.fader = (cfg) ->

    ###
    Set styles here
    ###
    slide =
      'position': 'absolute'
      'z-index': 0
      'opacity': 0

    slideCurrent =
      'z-index': 1
      'opacity': 1

    $(@).css slide
    $(@).eq(0).css slideCurrent

    methods =
      effect: (cur, prev) ->
        fadeIn =
          'opacity': 1
          'z-index': 2

        fadeOut =
          'opacity': 0
          'z-index': 0

        cur.animate fadeIn, cfg.speed
        if cfg.callback?
          prev.animate fadeOut, cfg.speed, cfg.callback $.data(_this, 'current')
        else
          prev.animate fadeOut, cfg.speed
        return

    ###
    Loop
    Set the loop going
    ###
    @loop 'start', cfg.timer, methods.effect

    ###
    Prev
    If we manually click the next button then stop the loop, go to the
    previous slide and then start the timer again
    ###
    if (cfg.prev)
      cfg.prev.click =>
        @loop 'prev'
        return

    ###
    Next
    If we manually click the next button then stop the loop, go to the
    next slide and then start the timer again
    ###
    if (cfg.next)
      cfg.next.click =>
        @loop 'next'
        return

  ###
  Slider
  When applied to a collection of elements, will slide the elements from
  right to left.
  ###
  $.fn.slider = (cfg) ->

    ###
    Set styles here
    ###
    containerStyle =
      'width': $(window).width()
      'position': 'absolute'
      'left': 0
      'overflow': 'hidden'

    wrapStyle =
      'width': _this.eq(0).width() * 3
      'margin-left': (($(window).width() - (_this.eq(0).width() * 3)) / 2) + 'px'

    slideStyle =
      'float': 'left'
      'opacity': 0

    @wrapAll '<div class="slider-wrap"></div>'
    @parent().css wrapStyle

    @parent().wrapAll '<div class="slider-container"></div>'
    @parent().parent().css containerStyle

    @parent().css wrapStyle
    $(slide).css slideStyle for slide in $(@)

    $(@).eq(1).css 'opacity', 1

    methods =
      prevEffect: (prev, cur) =>
          # Get the width of the element

          slideRight =
            'margin-left': 0

          fadeIn =
            'opacity': 1

          fadeOut =
            'opacity': 0

          loop_cfg =
            'method': 'get_next'

          next = @loop loop_cfg

          prev.css 'margin-left': 0 - prev.width()
          $(@).parent().prepend prev
          prev.animate slideRight

          cur.animate fadeIn
          (@).eq(next).animate fadeOut
          return

      nextEffect: (cur, prev) =>
          # Get the width of the element

          slideLeft =
            'margin-left': 0 - prev.width()

          fadeIn =
            'opacity': 1

          fadeOut =
            'opacity': 0

          loop_cfg =
            'method': 'get_next'

          next = @loop loop_cfg

          prev.animate slideLeft, ->
            $(@).parent().append @
            $(@).css 'margin-left', 0
            return

          cur.animate fadeOut
          (@).eq(next).animate fadeIn
          return

    loop_cfg =
      'method': 'start'
      'time': cfg.timer
      'nextEffect': methods.nextEffect
      'prevEffect': methods.prevEffect
      'callback': cfg.callback

    @loop loop_cfg

    ###
    Prev
    If we manually click the next button then stop the loop, go to the
    previous slide and then start the timer again
    ###
    if (cfg.prev)
      cfg.prev.click =>
        loop_cfg =
            'method': 'next'
        @loop loop_cfg
        return

    ###
    Next
    If we manually click the next button then stop the loop, go to the
    next slide and then start the timer again
    ###
    if (cfg.next)
      cfg.next.click =>
        loop_cfg =
            'method': 'prev'
        @loop loop_cfg
        return

  ###
  Cycle
  Can be used to call one of the loop effects
  ###
  $.fn.cycle = (cfg) ->
    @[cfg.effect] cfg
    return

  ###
  Modal
  When applied to an element, adds a modal effect to it
  ###
  $.fn.modal = ->

  ###
  Snap To
  When applied to an element will cause the window to snap to it
  ###
  $.fn.snap = (cfg) ->
    snap_points = $(window).data 'snap_points'
    if snap_points?
      snap_points.push $(@)
    else
      snap_points = new Array($(@))
    $(window).data 'snap_points', snap_points

  $(window).scroll ->

    # Overwrite a timeout
    clearTimeout $(window).data 'snap_timeout'
    $(window).data 'snap_timeout', setTimeout(window.bm.snap, 500)

  return
) jQuery